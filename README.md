# Co zawiera projekt #

Jest to przykład testu funkcjonalnego z użyciem frameworka Selenium. Do projektu wykorzystana została strona: http://automationpractice.com.
Test przeprowadza pełny proces zakupu produktu wraz z stworzeniem konta.

* Do nawigacji jest wykorzystywany głównie język xPath.

By uruchomić projket w VS należy dodać pakiety NuGet:

* Selenium.WebDriver
* Selenium.WebDriver.GeckoDriver
* Selenium.Support
* NUnit