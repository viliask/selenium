﻿using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.IO;

namespace selenium

{
    class Program
    {
        public static object TimeUnit { get; private set; }

        static void Main(string[] args)
        {
            IWebDriver driver = new FirefoxDriver();

            driver.Url = ("http://automationpractice.com/index.php");

            driver.FindElement(
              By.XPath(
                "/html/body/div[1]/div[1]/header/div[3]/div/div/div[6]/ul/li[2]/a"
              )
            ).Click();

                TimeSpan ts = TimeSpan.FromSeconds(5);

                WebDriverWait wait = new WebDriverWait(driver, ts);

                wait.Until(ExpectedConditions.PresenceOfAllElementsLocatedBy(By.XPath("/html/body/div[1]/div[2]/div/div[3]/div[2]/ul")));

                driver.FindElement(
                  By.XPath(
                    "/html/body/div[1]/div[2]/div/div[3]/div[2]/ul/li[5]/div/div[1]/div/a[1]/img"
                  )
                ).Click();

                    driver.FindElement(By.Id("group_1")).SendKeys("M");

                    driver.FindElement(By.Name("Green")).Click();

                    driver.FindElement(
                      By.XPath(
                        "/html/body/div[1]/div[2]/div/div[3]/div/div/div/div[4]/form/div/div[3]/div[1]/p/button"
                      )
                    ).Click();

                    ts = TimeSpan.FromSeconds(10);

                    wait = new WebDriverWait(driver, ts);

                    wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.VisibilityOfAllElementsLocatedBy(By.XPath("/html/body/div[1]/div[1]/header/div[3]/div/div/div[4]/div[1]/div[2]/div[4]")));
            
            ////kupujemy

            driver.FindElement(
              By.XPath(
                "/html/body/div[1]/div[1]/header/div[3]/div/div/div[4]/div[1]/div[2]/div[4]/a/span"
              )
            ).Click();

            System.Threading.Thread.Sleep(5000);

            driver.FindElement(
              By.XPath(
                "/html/body/div[1]/div[2]/div/div[3]/div/p[2]/a[1]/span"
              )
            ).Click();


            driver.FindElement(By.Id("email_create")).SendKeys("myemail@gmail.com");

            driver.FindElement(
              By.XPath(
                "//*[@id='SubmitCreate']"
              )
            ).Click();

            System.Threading.Thread.Sleep(5000);


            driver.FindElement(By.Id("email_create")).Clear();

            driver.FindElement(By.Id("email_create")).SendKeys("myemail6436ghf76@gmail.com");

            driver.FindElement(
              By.XPath(
                "//*[@id='SubmitCreate']"
              )
            ).Click();


            System.Threading.Thread.Sleep(5000);

            ts = TimeSpan.FromSeconds(10);

            wait = new WebDriverWait(driver, ts);

            wait.Until(ExpectedConditions.PresenceOfAllElementsLocatedBy(By.XPath("//*[@id='submitAccount']")));


            driver.FindElement(
              By.XPath(
                "/html/body/div[1]/div[2]/div/div[3]/div/div/form/div[1]/div[1]/div[1]/label"
              )
            ).Click();


                driver.FindElement(By.Id("customer_firstname")).SendKeys("Hans");

                driver.FindElement(By.Id("customer_lastname")).SendKeys("Geiger");

                driver.FindElement(By.Id("passwd")).SendKeys("secretpasswd");

                driver.FindElement(By.Id("days")).SendKeys("6");

                driver.FindElement(By.Id("months")).SendKeys("May");

                driver.FindElement(By.Id("years")).SendKeys("1980");

                driver.FindElement(By.Id("address1")).SendKeys("Lecha Kaczynskiego 6/66, 66-666, Kaczkowo");

                System.Threading.Thread.Sleep(5000);

                driver.FindElement(By.Id("city")).SendKeys("Kaczkowo");

                driver.FindElement(By.Id("id_state")).SendKeys("California");

                driver.FindElement(By.Id("phone_mobile")).SendKeys("666-666-666");

                driver.FindElement(By.Id("alias")).SendKeys("My secret adress");

                driver.FindElement(
                  By.XPath(
                    "//*[@id='submitAccount']"
                  )
                ).Click();

                ts = TimeSpan.FromSeconds(10);

                wait = new WebDriverWait(driver, ts);

                wait.Until(ExpectedConditions.PresenceOfAllElementsLocatedBy(By.XPath("//*[@id='submitAccount']")));

                System.Threading.Thread.Sleep(5000);

                driver.FindElement(By.Id("postcode")).SendKeys("66666");

                driver.FindElement(By.Id("passwd")).SendKeys("secretpasswd");

                driver.FindElement(
                  By.XPath(
                    "//*[@id='submitAccount']"
                  )
                ).Click();

                driver.FindElement(By.Name("processAddress")).Click();


                    ts = TimeSpan.FromSeconds(10);

                    wait = new WebDriverWait(driver, ts);

                    wait.Until(ExpectedConditions.PresenceOfAllElementsLocatedBy(By.XPath("/html/body/div[1]/div[2]/div/div[3]/div/div/form/div/div[2]/div[1]/div/div/table/tbody/tr/td[2]/img")));

                    driver.FindElement(
                      By.XPath(
                        "//*[@id='cgv']"
                      )
                    ).Click();

                    System.Threading.Thread.Sleep(5000);

                    driver.FindElement(By.Name("processCarrier")).Click();

                    ts = TimeSpan.FromSeconds(10);

                    wait = new WebDriverWait(driver, ts);

                    wait.Until(ExpectedConditions.PresenceOfAllElementsLocatedBy(By.XPath("/html/body/div[1]/div[2]/div/div[3]/div/div/div[3]/div[1]/div/p/a")));


                    driver.FindElement(
                      By.XPath(
                        "/html/body/div[1]/div[2]/div/div[3]/div/div/div[3]/div[1]/div/p/a"
                      )
                    ).Click();

            System.Threading.Thread.Sleep(5000);


                        ts = TimeSpan.FromSeconds(10);

                        wait = new WebDriverWait(driver, ts);

                        wait.Until(ExpectedConditions.PresenceOfAllElementsLocatedBy(By.XPath("/html/body/div[1]/div[2]/div/div[3]/div/form/p/button")));


                        driver.FindElement(
                          By.XPath(
                            "/html/body/div[1]/div[2]/div/div[3]/div/form/p/button"
                          )
                        ).Click();
            //[@class='sf-with-ul' and @title='Dresses']

            //driver.FindElement(By.Titl("sf-with-ul")).Click();

            //driver.FindElement(By.CssSelector("input[class='form-control input-sm']")).Clear();
            //driver.FindElement(By.CssSelector("input[class='form-control input-sm']")).SendKeys("5");

            //driver.FindElement(By.Name("submit")).Click();



            /*
             * 
             * 
             driver.FindElement(By.XPath("//*[@id='blockcart_caroucel']/li[1]")).Click();

            driver.FindElement(By.Id("group_1")).SendKeys("M");


            driver.FindElement(
              By.XPath(
                "/html/body/div[1]/div[2]/div/div[3]/div/div/div/div[4]/form/div/div[3]/div[1]/p/button"
              )
            ).Click();
            ///

            ts = TimeSpan.FromSeconds(10);

            wait = new WebDriverWait(driver, ts);

            wait.Until(ExpectedConditions.PresenceOfAllElementsLocatedBy(By.XPath("/html/body/div[1]/div[1]/header/div[3]/div/div/div[4]/div[1]/div[2]/div[4]/a/span")));

            IList<IWebElement> List = driver.FindElements(By.ClassName("support-phrase"));

            
            foreach (var item in List)
            {
                NewList.Add(item.Text);
                Console.WriteLine(item.Text);
            }

                Console.WriteLine();
                Console.WriteLine("Jak Ci sie znudzilo, to wpisz 'q'");
                temp = (Console.ReadLine());
            

            TextWriter tw = new StreamWriter("listalosowychfraz.txt");

            foreach (String s in NewList)
                tw.WriteLine(s);

            tw.Close();

            Console.WriteLine();
            Console.WriteLine("Zapisano w pliku: listalosowychfraz.txt");

            driver.Url = ("https://facebook.com");
            */
            /*
            driver.Url = ("http://toolsqa.wpengine.com/Automation-practice-form/");

            driver.FindElement(By.Name("firstname")).SendKeys("Hehehehehe");

            driver.FindElement(By.Name("lastname")).SendKeys("Lololol");

            driver.FindElement(By.Id("sex-0")).Click();

            driver.FindElement(By.Id("exp-6")).Click();

            driver.FindElement(By.Id("datepicker")).SendKeys("Hihihihi");

            driver.FindElement(By.Id("photo")).SendKeys(@"C:\Users\Nalesniki\Desktop\DSC_1051.jpg");

            driver.FindElement(By.Id("profession-1")).Click();

            driver.FindElement(By.Id("continents")).SendKeys("Antartica");
            */
        }

    }
}
